functions{
    vector merge_missing( array[] int miss_indexes , vector x_obs , vector x_miss ) {
        int N = dims(x_obs)[1];
        int N_miss = dims(x_miss)[1];
        vector[N] merged;
        merged = x_obs;
        for ( i in 1:N_miss )
            merged[ miss_indexes[i] ] = x_miss[i];
        return merged;
    }
}

data{
    int N;
    int K;
    int N_height_missidx;
    int N_weight_missidx;
    array[N_height_missidx] int height_missidx;
    array[N_weight_missidx] int weight_missidx;
    array[N] int pathology;
    vector[N] AA;
    vector[N] APA;
    vector[N] ATA;
    vector[N] GIA;
    vector[N] GVA;
    vector[N] weight;
    vector[N] height;
    vector[N] age;
    array[N] int sex;
    int <lower=0, upper=1> priorOnly;
    real averagePrior;
    real averagePriorSd;
    real ageEffectPrior;
    real ageEffectPriorSd;
    real WHEffectPrior;
    real WHEffectPriorSd;
    real anatomyEffectPrior;
    real anatomyEffectPriorSd;
}
parameters{
    vector[K-1] aFemale;
    vector[K-1] aMale;
    vector[K-1] bAgeFemale;
    vector[K-1] bAgeMale;
    vector[K-1] bWFemale;
    vector[K-1] bWMale;
    vector[K-1] bHFemale;
    vector[K-1] bHMale;
    vector[K-1] bAAFemale;
    vector[K-1] bAAMale;
    vector[K-1] bATAFemale;
    vector[K-1] bATAMale;
    vector[K-1] bAPAFemale;
    vector[K-1] bAPAMale;
    vector[K-1] bGIAFemale;
    vector[K-1] bGIAMale;
    vector[K-1] bGVAFemale;
    vector[K-1] bGVAMale;
    vector[2] aH;
    vector[2] bAgeH;
    vector[2] aW;
    vector[2] bAgeW;
    vector[2] bHeightW;
    real<lower=0> sigmaH;
    real<lower=0> sigmaW;
    vector[N_height_missidx] height_impute;
    vector[N_weight_missidx] weight_impute;
}
model{
    vector[K] p;
    vector[K] s;
    vector[N] height_merge;
    vector[N] weight_merge;
    vector[N] muH;
    vector[N] muW;
    sigmaW ~ exponential( 1 );
    sigmaH ~ exponential( 1 );
    bHeightW ~ normal( 0 , 1 );
    bAgeW ~ normal( 0 , 0.1 );
    aW ~ normal( 70 , 5 );
    bAgeH ~ normal( 0 , 0.1 );
    aH ~ normal( 160 , 10 );
    
    bAAFemale ~ normal( anatomyEffectPrior , anatomyEffectPriorSd );
    bAAMale ~ normal( anatomyEffectPrior , anatomyEffectPriorSd );
    
    bATAFemale ~ normal( anatomyEffectPrior , anatomyEffectPriorSd );
    bATAMale ~ normal( anatomyEffectPrior , anatomyEffectPriorSd );
    
    bAPAFemale ~ normal( anatomyEffectPrior , anatomyEffectPriorSd );
    bAPAMale ~ normal( anatomyEffectPrior , anatomyEffectPriorSd );
    
    bGIAFemale ~ normal( anatomyEffectPrior , anatomyEffectPriorSd );
    bGIAMale ~ normal( anatomyEffectPrior , anatomyEffectPriorSd );
    
    bGVAFemale ~ normal( anatomyEffectPrior , anatomyEffectPriorSd );
    bGVAMale ~ normal( anatomyEffectPrior , anatomyEffectPriorSd );
    
    bHFemale ~ normal( WHEffectPrior , WHEffectPriorSd );
    bHMale ~ normal( WHEffectPrior , WHEffectPriorSd );
    
    bWFemale ~ normal( WHEffectPrior , WHEffectPriorSd );
    bWMale ~ normal( WHEffectPrior , WHEffectPriorSd );
    
    bAgeFemale ~ normal( ageEffectPrior , ageEffectPriorSd );
    bAgeMale ~ normal( ageEffectPrior , ageEffectPriorSd );
    
    aFemale ~ normal( averagePrior , averagePriorSd );
    aMale ~ normal( averagePrior , averagePriorSd );
    
    for ( i in 1:N ) {
        muH[i] = aH[sex[i]] + bAgeH[sex[i]] * (age[i] - 70);
    }
    height_merge = merge_missing(height_missidx, to_vector(height), height_impute);
    height_merge ~ normal( muH , sigmaH );
    
    for ( i in 1:N ) {
        muW[i] = aW[sex[i]] + bAgeW[sex[i]] * (age[i] - 70) + bHeightW[sex[i]] * (height_merge[i] - 170);
    }
    weight_merge = merge_missing(weight_missidx, to_vector(weight), weight_impute);
    weight_merge ~ normal( muW , sigmaW );
    
    if (!priorOnly){
        for ( i in 1:N ) {
          for (j in 1:(K-1)) {
            if (sex[i] == 1)
              s[j] = aFemale       [j] +
                     bAgeFemale    [j] * (age[i] - 70) +
                     bWFemale      [j] * (weight_merge[i] - 75) +
                     bHFemale      [j] * (height_merge[i] - 170) +
                     bAAFemale     [j] * (AA[i] - 25) + 
                     bATAFemale    [j] * (ATA[i] - 27) +
                     bAPAFemale    [j] * (APA[i] - 13) + 
                     bGIAFemale    [j] * (GIA[i] - 8) + 
                     bGVAFemale    [j] * (GVA[i] - 0) ;
            else
              s[j] = aMale       [j] +
                     bAgeMale    [j] * (age[i] - 70) +
                     bWMale      [j] * (weight_merge[i] - 75) +
                     bHMale      [j] * (height_merge[i] - 170) +
                     bAAMale     [j] * (AA[i] - 25) + 
                     bATAMale    [j] * (ATA[i] - 27) +
                     bAPAMale    [j] * (APA[i] - 13) + 
                     bGIAMale    [j] * (GIA[i] - 8) + 
                     bGVAMale    [j] * (GVA[i] - 0) ;
        }
        s[K] = 0;
        pathology[i] ~ categorical_logit(s);
    }
}
}
generated quantities{
    vector[N] log_lik;
    vector[K] s;
    vector[K] p;
    vector[N] muH;
    vector[N] muW;
    vector[N] weight_merge;
    vector[N] height_merge;
    
    for ( i in 1:N ) {
        muH[i] = aH[sex[i]] + bAgeH[sex[i]] * (age[i] - 70);
    }
    height_merge = merge_missing(height_missidx, to_vector(height), height_impute);
     
    for ( i in 1:N ) {
        muW[i] = aW[sex[i]] + bAgeW[sex[i]] * (age[i] - 70) + bHeightW[sex[i]] * (height[i] - 170);
    }
    weight_merge = merge_missing(weight_missidx, to_vector(weight), weight_impute);
    
    for ( i in 1:N ) {
          for (j in 1:(K-1)) {
            if (sex[i] == 1)
              s[j] = aFemale       [j] +
                     bAgeFemale    [j] * (age[i] - 70) +
                     bWFemale      [j] * (weight_merge[i] - 75) +
                     bHFemale      [j] * (height_merge[i] - 170) +
                     bAAFemale     [j] * (AA[i] - 25) + 
                     bATAFemale    [j] * (ATA[i] - 27) +
                     bAPAFemale    [j] * (APA[i] - 13) + 
                     bGIAFemale    [j] * (GIA[i] - 8) + 
                     bGVAFemale    [j] * (GVA[i] - 0) ;
            else
              s[j] = aMale       [j] +
                     bAgeMale    [j] * (age[i] - 70) +
                     bWMale      [j] * (weight_merge[i] - 75) +
                     bHMale      [j] * (height_merge[i] - 170) +
                     bAAMale     [j] * (AA[i] - 25) + 
                     bATAMale    [j] * (ATA[i] - 27) +
                     bAPAMale    [j] * (APA[i] - 13) + 
                     bGIAMale    [j] * (GIA[i] - 8) + 
                     bGVAMale    [j] * (GVA[i] - 0) ;
        }
        s[K] = 0;
        p = softmax(s);
        log_lik[i] = categorical_lpmf( pathology[i] | p );
    }
}

