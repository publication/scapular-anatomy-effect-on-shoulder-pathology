functions{
    vector merge_missing( array[] int miss_indexes , vector x_obs , vector x_miss ) {
        int N = dims(x_obs)[1];
        int N_miss = dims(x_miss)[1];
        vector[N] merged;
        merged = x_obs;
        for ( i in 1:N_miss )
            merged[ miss_indexes[i] ] = x_miss[i];
        return merged;
    }
}

data{
    int N;
    int N_height_missidx;
    int N_weight_missidx;
    array[N_height_missidx] int height_missidx;
    array[N_weight_missidx] int weight_missidx;
    vector[N] AA;
    vector[N] GIA;
    vector[N] weight;
    vector[N] height;
    vector[N] age;
    array[N] int sex;
    int <lower=0, upper=1> priorOnly;
}
parameters{
    vector[2] aAvg;
    vector[2] bAge;
    vector[2] bHeight;
    vector[2] bWeight;
    vector[2] bAA;
    vector[2] aH;
    vector[2] bAgeH;
    vector[2] aW;
    vector[2] bAgeW;
    vector[2] bHeightW;
    real<lower=0> sigmaH;
    real<lower=0> sigmaW;
    real<lower=0> sigma;
    vector[N_height_missidx] height_impute;
    vector[N_weight_missidx] weight_impute;
}
model{
    vector[N] mu;
    vector[N] height_merge;
    vector[N] weight_merge;
    vector[N] muH;
    vector[N] muW;
    
    sigmaW ~ exponential( 1 );
    sigmaH ~ exponential( 1 );
    bHeightW ~ normal( 0 , 1 );
    bAgeW ~ normal( 0 , 0.1 );
    aW ~ normal( 70 , 5 );
    bAgeH ~ normal( 0 , 0.1 );
    aH ~ normal( 160 , 10 );
    
    sigma ~ exponential(1);
    aAvg ~ normal(8, 5);
    bAge ~ normal(0, 1);
    bHeight ~ normal(0, 1);
    bWeight ~ normal(0, 1);
    bAA ~ normal(0, 1);
    
    
    for ( i in 1:N ) {
        muH[i] = aH[sex[i]] + bAgeH[sex[i]] * (age[i] - 70);
    }
    height_merge = merge_missing(height_missidx, to_vector(height), height_impute);
    height_merge ~ normal( muH , sigmaH );
    
    for ( i in 1:N ) {
        muW[i] = aW[sex[i]] + bAgeW[sex[i]] * (age[i] - 70) + bHeightW[sex[i]] * (height_merge[i] - 160);
    }
    weight_merge = merge_missing(weight_missidx, to_vector(weight), weight_impute);
    weight_merge ~ normal( muW , sigmaW );
    
    if (!priorOnly){
        for ( i in 1:N ) {
          
          mu[i] = aAvg[sex[i]] +
                  bAge[sex[i]]*(age[i]-70) +
                  bHeight[sex[i]]*(height_merge[i]-170) +
                  bWeight[sex[i]]*(weight_merge[i]-75) +
                  bAA[sex[i]]*(AA[i]-25);
    }
        GIA ~ normal(mu, sigma);
}
}
generated quantities{
    vector[N] log_lik;
    vector[N] muH;
    vector[N] muW;
    vector[N] mu;
    vector[N] weight_merge;
    vector[N] height_merge;
    
    for ( i in 1:N ) {
        muH[i] = aH[sex[i]] + bAgeH[sex[i]] * (age[i] - 70);
    }
    height_merge = merge_missing(height_missidx, to_vector(height), height_impute);
     
    for ( i in 1:N ) {
        muW[i] = aW[sex[i]] + bAgeW[sex[i]] * (age[i] - 70) + bHeightW[sex[i]] * (height[i] - 170);
    }
    weight_merge = merge_missing(weight_missidx, to_vector(weight), weight_impute);
    
    for ( i in 1:N ) {
      
        mu[i] = aAvg[sex[i]] +
                bAge[sex[i]]*(age[i]-70) +
                bHeight[sex[i]]*(height_merge[i]-170) +
                bWeight[sex[i]]*(weight_merge[i]-75) +
                bAA[sex[i]]*(AA[i]-25);
      
    }
    
    for (i in 1:N) log_lik[i] = normal_lpdf(GIA[i] | mu[i], sigma);
}

