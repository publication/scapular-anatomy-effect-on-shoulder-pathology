data{
    int N;
    int K;
    array[N] int pathology;
    vector[N] age;
    array[N] int sex;
    int <lower=0, upper=1> priorOnly;
    real averagePrior;
    real averagePriorSd;
    real ageEffectPrior;
    real ageEffectPriorSd;
}
parameters{
    vector[K-1] aFemale;
    vector[K-1] aMale;
    vector[K-1] bAgeFemale;
    vector[K-1] bAgeMale;
}
model{
    vector[K] p;
    vector[K] s;
    
    bAgeFemale ~ normal( ageEffectPrior , ageEffectPriorSd );
    bAgeMale ~ normal( ageEffectPrior , ageEffectPriorSd );
    
    aFemale ~ normal( averagePrior , averagePriorSd );
    aMale ~ normal( averagePrior , averagePriorSd );
    
    if (!priorOnly){
        for ( i in 1:N ) {
          for (j in 1:(K-1)) {
            if (sex[i] == 1)
              s[j] = aFemale       [j] +
                     bAgeFemale    [j] * (age[i] - 70) ;
            else
              s[j] = aMale       [j] +
                     bAgeMale    [j] * (age[i] - 70) ;
        }
        s[K] = 0;
        pathology[i] ~ categorical_logit(s);
    }
}
}
generated quantities{
    vector[N] log_lik;
    vector[K] s;
    vector[K] p;
    
    for ( i in 1:N ) {
          for (j in 1:(K-1)) {
            if (sex[i] == 1)
              s[j] = aFemale       [j] +
                     bAgeFemale    [j] * (age[i] - 70) ;
            else
              s[j] = aMale       [j] +
                     bAgeMale    [j] * (age[i] - 70) ;
        }
        s[K] = 0;
        p = softmax(s);
        log_lik[i] = categorical_lpmf( pathology[i] | p );
    }
}

