functions{
    vector merge_missing( array[] int miss_indexes , vector x_obs , vector x_miss ) {
        int N = dims(x_obs)[1];
        int N_miss = dims(x_miss)[1];
        vector[N] merged;
        merged = x_obs;
        for ( i in 1:N_miss )
            merged[ miss_indexes[i] ] = x_miss[i];
        return merged;
    }
}

data{
    int N;
    int K;
    int N_height_missidx;
    int N_weight_missidx;
    array[N_height_missidx] int height_missidx;
    array[N_weight_missidx] int weight_missidx;
    array[N] int pathology;
    vector[N] height;
    vector[N] weight;
    vector[N] age;
    array[N] int sex;
    int <lower=0, upper=1> priorOnly;
    real averagePrior;
    real averagePriorSd;
    real ageEffectPrior;
    real ageEffectPriorSd;
    real HWEffectPrior;
    real HWEffectPriorSd;
}
parameters{
    vector[K-1] aFemale;
    vector[K-1] aMale;
    vector[K-1] bAgeFemale;
    vector[K-1] bAgeMale;
    vector[K-1] bHFemale;
    vector[K-1] bHMale;
    vector[K-1] bWFemale;
    vector[K-1] bWMale;
    vector[2] aH;
    vector[2] bAgeH;
    vector[2] aW;
    vector[2] bAgeW;
    vector[2] bHeightW;
    real<lower=0> sigmaH;
    real<lower=0> sigmaW;
    vector[N_height_missidx] height_impute;
    vector[N_weight_missidx] weight_impute;
}
model{
    vector[K] p;
    vector[K] s;
    vector[N] height_merge;
    vector[N] weight_merge;
    vector[N] muH;
    vector[N] muW;
    sigmaW ~ exponential( 1 );
    sigmaH ~ exponential( 1 );
    bHeightW ~ normal( 0 , 1 );
    bAgeW ~ normal( 0 , 0.1 );
    aW ~ normal( 70 , 5 );
    bAgeH ~ normal( 0 , 0.1 );
    aH ~ normal( 160 , 10 );
    
    bHFemale ~ normal( HWEffectPrior , HWEffectPriorSd );
    bHMale ~ normal( HWEffectPrior , HWEffectPriorSd );
    
    bWFemale ~ normal( HWEffectPrior , HWEffectPriorSd );
    bWMale ~ normal( HWEffectPrior , HWEffectPriorSd );
    
    bAgeFemale ~ normal( ageEffectPrior , ageEffectPriorSd );
    bAgeMale ~ normal( ageEffectPrior , ageEffectPriorSd );
    
    aFemale ~ normal( averagePrior , averagePriorSd );
    aMale ~ normal( averagePrior , averagePriorSd );
    
    for ( i in 1:N ) {
        muH[i] = aH[sex[i]] + bAgeH[sex[i]] * (age[i] - 70);
    }
    height_merge = merge_missing(height_missidx, to_vector(height), height_impute);
    height_merge ~ normal( muH , sigmaH );
    
    for ( i in 1:N ) {
        muW[i] = aW[sex[i]] + bAgeW[sex[i]] * (age[i] - 70) + bHeightW[sex[i]] * (height_merge[i] - 170);
    }
    weight_merge = merge_missing(weight_missidx, to_vector(weight), weight_impute);
    weight_merge ~ normal( muW , sigmaW );
    
    if (!priorOnly){
        for ( i in 1:N ) {
          for (j in 1:(K-1)) {
            if (sex[i] == 1)
              s[j] = aFemale       [j] +
                     bAgeFemale    [j] * (age[i] - 70) +
                     bHFemale      [j] * (height_merge[i] - 170) +
                     bWFemale      [j] * (weight_merge[i] - 75) ;
            else
              s[j] = aMale       [j] +
                     bAgeMale    [j] * (age[i] - 70) +
                     bHMale      [j] * (height_merge[i] - 170) +
                     bWMale      [j] * (weight_merge[i] - 75) ;
        }
        s[K] = 0;
        pathology[i] ~ categorical_logit(s);
    }
}
}
generated quantities{
    vector[N] log_lik;
    vector[K] s;
    vector[K] p;
    vector[N] muH;
    vector[N] muW;
    vector[N] weight_merge;
    vector[N] height_merge;
    
    for ( i in 1:N ) {
        muH[i] = aH[sex[i]] + bAgeH[sex[i]] * (age[i] - 70);
    }
    height_merge = merge_missing(height_missidx, to_vector(height), height_impute);
    
    for ( i in 1:N ) {
        muW[i] = aW[sex[i]] + bAgeW[sex[i]] * (age[i] - 70) + bHeightW[sex[i]] * (height[i] - 170);
    }
    weight_merge = merge_missing(weight_missidx, to_vector(weight), weight_impute);
    
    for ( i in 1:N ) {
          for (j in 1:(K-1)) {
            if (sex[i] == 1)
              s[j] = aFemale       [j] +
                     bAgeFemale    [j] * (age[i] - 70) +
                     bHFemale      [j] * (height_merge[i] - 170) +
                     bWFemale      [j] * (weight_merge[i] - 75) ;
            else
              s[j] = aMale       [j] +
                     bAgeMale    [j] * (age[i] - 70) +
                     bHMale      [j] * (height_merge[i] - 170) +
                     bWMale      [j] * (weight_merge[i] - 75) ;
        }
        s[K] = 0;
        p = softmax(s);
        log_lik[i] = categorical_lpmf( pathology[i] | p );
    }
}

