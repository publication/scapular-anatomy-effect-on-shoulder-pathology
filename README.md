# Causal associations between scapular morphology and shoulder pathology estimated with Bayesian statistics study.

## Introduction
This repository contains R and Stan codes to build/import a Bayesian model for assessing the effect of scapular anatomy on shoulder pathology, adjusting for age, sex, height and weight.

## Setup

### Download

Clone or download this repository.
Open scapular-anatomy-effect-on-shoulder-pathology.Rproj file.
Open bone_anatomy_effect_on_pathology.Rmd file.
Install the necessary libraries and run every section which saves the results in the fig directory.

## How to use
You need some R libraries for this code which you can see on the first section of the shoulder_pathology_effect_on_glenoid_BMD.Rmd file.

For rethinking package you need to install some libraries before.

Please follow the installation tutorial for this package here https://github.com/rmcelreath/rethinking


### Access data and models

The necessary data for this study are located in the data directory. Stan code of the Bayesian models is located in the models directory.

### Visualize data

All the graphs are saved automatically and you can change their names inside the bone_anatomy_effect_on_pathology.Rmd file.

## Files/Folders

### Files
- age_on_patho_link.R is a function which simulates the effect of the input variables on the outcome (pathology probability) based on the sex and age effect on pathology Bayesian model.
- anatomy_on_patho_link.R is a function which simulates the effect of the input variables on the outcome (pathology probability) based on the anatomy effect on pathology Bayesian model.
- anatomy_var_on_patho_effect_plot.R is a function which saves a graph from the anatomy effect on pathology Bayesian model, with the exposure on x-axis and pathology probability on the y-axis.
- height_on_patho_link.R is a function which simulates the effect of the input variables on the outcome (pathology probability) based on the height effect on pathology Bayesian model.
- prior_predictive_simulation_plot.R is a function which saves a graph for the prior predictive simulation of Bayesian models.
- weight_on_patho_link.R is a function which simulate the effect of the input variables on the outcome (pathology probability) based on the weight effect on pathology Bayesian model.


### Folders
- **./data contains all the input data for the code.
- **./fig contains all of the generated figures from code.
- **./models contains the stan code of the Bayesian models.


## Contributors
* Pezhman Eghbalishamsabadi, 2024-now.


## License

EPFL CC BY-NC-SA 4.0 Int.
