weight_on_patho_link <- function( post , data ) {
  
  if (data$sex[1] == 1){
    
    value_oa <- with( post ,
                      sapply( 1:length(data$age) ,
                              function(i) aFemale[,1] +
                                          bAgeFemale[,1] * (data$age[i] - 70) +
                                          bHFemale[,1] * (data$height[i] - 170) +
                                          bWFemale[,1] * (data$weight[i] - 75)
                      ))
    
    value_cta <- with( post ,
                       sapply( 1:length(data$age) ,
                               function(i) aFemale[,2] +
                                    bAgeFemale[,2] * (data$age[i] - 70) +
                                    bHFemale[,2] * (data$height[i] - 170) +
                                    bWFemale[,2] * (data$weight[i] - 75)
                       ))
    
    value_ctrl <- with( post ,
                        sapply( 1:length(data$age) ,
                                function(i) aFemale[,3] +
                                  bAgeFemale[,3] * (data$age[i] - 70) +
                                  bHFemale[,3] * (data$height[i] - 170) +
                                  bWFemale[,3] * (data$weight[i] - 75)
                        ))
    
  } else if (data$sex[1] == 2){
    
    
    value_oa <- with( post ,
                      sapply( 1:length(data$age) ,
                              function(i) aMale[,1] +
                                bAgeMale[,1] * (data$age[i] - 70) +
                                bHMale[,1] * (data$height[i] - 170) +
                                bWMale[,1] * (data$weight[i] - 75)
                      ))
    
    value_cta <- with( post ,
                       sapply( 1:length(data$age) ,
                               function(i) aMale[,2] +
                                 bAgeMale[,2] * (data$age[i] - 70) +
                                 bHMale[,2] * (data$height[i] - 170) +
                                 bWMale[,2] * (data$weight[i] - 75)
                       ))
    
    value_ctrl <- with( post ,
                        sapply( 1:length(data$age) ,
                                function(i) aMale[,3] +
                                  bAgeMale[,3] * (data$age[i] - 70) +
                                  bHMale[,3] * (data$height[i] - 170) +
                                  bWMale[,3] * (data$weight[i] - 75)
                        ))
    
  }  
  
  value_other <- matrix(data=0, nrow=nrow(value_oa), ncol=ncol(value_oa))
  
  all_mat <- cbind(c(value_oa), c(value_cta), c(value_ctrl), c(value_other))
  
  result <- apply(all_mat, MARGIN=1, FUN=softmax)
  
  oa_prob    <- matrix(result[1,], nrow = nrow(value_oa)   , ncol = ncol(value_oa)   )
  cta_prob   <- matrix(result[2,], nrow = nrow(value_cta)  , ncol = ncol(value_cta)  )
  ctrl_prob  <- matrix(result[3,], nrow = nrow(value_ctrl) , ncol = ncol(value_ctrl) )
  other_prob <- matrix(result[4,], nrow = nrow(value_other), ncol = ncol(value_other))
  
  return( list( oa_prob = oa_prob ,
                cta_prob = cta_prob ,
                ctrl_prob = ctrl_prob ,
                other_prob = other_prob) )
  
}
